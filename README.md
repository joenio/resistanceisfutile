---
pagetitle: RESISTANCE IS FUTILE
bibliography: resistanceisfutile.bib
---

# RESISTANCE IS FUTILE

> You will be assimilated! - The Borg

![](resistance-is-futile.svg)

## What?

```
minimalist
ephemeral
street
art
```

## Why?

![](00-20210927-paris/IMG_20210927_140117.jpg)

## How?

![](01-20220415-paris/IMG-20220416-WA0007.jpg)

![](02-20220517-berlin/IMG-20220517-WA0001.jpg)

![](03-20220521-berlin/IMG_20220521_145918_edit_155836166253824.jpg)

### How? (but technically)

Convert this Markdown document to HTML.

```sh
make index.html
```

Requires [Pandoc][] 2.17.1.1 or later and [citeproc][].

Convert to PDF.

```sh
make resistanceisfutile.pdf
```

Requires the [rsvg-convert][] tool provided by Debian package [librsvg2-bin][].

## When and where?

### #01 - 15/04/2022, Paris

At bus stop boulevard Saint-Marcel.

![](01-20220415-paris/IMG_20220415_230013.jpg)

### #02 - 17/05/2022, Berlin

At square Alexanderplatz, Berlin.

![](02-20220517-berlin/IMG_20220517_125740.jpg)

### #03 - 21/05/2022, Berlin

At Karl-Liebknecht-Strabe.

![](03-20220521-berlin/IMG_20220521_233228.jpg)

### #04 - 17/06/2023, Praia do Forte

At Sapiranga, Mata de São João, Bahia.

![](04-20230617-sapiranga/IMG_1711.jpg)

### #05 - 30/06/2023, Salvador

At Mercadao.CC, Rio Vermelho, Salvador, Bahia.

![](05-20230630-mercadao-salvador/IMG_2011.jpg)

### #06 - 03/07/2023, Salvador

At UFBA, Salvador, Bahia.

![](06-20230703-salvador-ufba/IMG_20230703_200301.jpg)

### #07 - 09/07/2023, Morro do Chapéu

At cachoeira do Ferro Doido, Morro do Chapéu, Bahia.

![](07-20230709-morrodochapeu/IMG_2245.jpg)

### #08 - 10/07/2023, Jacobina

At Jacobina, Bahia.

![](08-20230710-jacobina/IMG_2269.jpg)

### #09 - 13/07/2023, Jacobina

At Jacobina, Bahia.

![](09-20230713-jacobina-stencil-com-pastel/IMG_2379.jpg)

## Who?

The Borg are cyborgs linked in a hive mind called "The Collective" from the
Star Trek fictional universe.

The term _Cyborg_ (cybernetic organisms) was coinded in 1960 on the  _Cyborgs
and space_ [@clynes1960cyborgs].

> Altering man's bodily functions to meet the requirements of extraterrestrial
> environments would be more logical than providing an earthly environment for
> him in space . . . Artifact-organism systems which would extend man's
> unconscious, self-regulatory controls are one possibility.<br/>
> <div style="text-align:right">-- Cyborgs and space [@clynes1960cyborgs]</div>

The Borg's ultimate goal is "achieving perfection" by co-opting the technology
and knowledge of other alien species to "The Collective" through the process of
"assimilation".

The Borg and the Big Tech companies share the same goal of
"achieving perfection" and share the same behavior through the process of
"assimilation".

## Changelog

### 0.2.0 - assimilation number two

- add photos during the [tour23](https://tour23.4two.art)

### 0.1.0 - assimilation number one

- first deploy at resistanceisfutile.4two.art

## Authors
 
- Joenio Marques da Costa <joenio@joenio.me>
- Mari Moura <marimoura.ministra@gmail.com>

## License

GPL-v3+

## Bibliography

[pandoc]: https://pandoc.org
[citeproc]: https://github.com/jgm/citeproc
[rsvg-convert]: https://manpages.debian.org/experimental/librsvg2-bin/rsvg-convert.1.en.html
[librsvg2-bin]: https://packages.debian.org/bookworm/librsvg2-bin
