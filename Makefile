PANDOC ?= pandoc

help:
	cat README.md

index.html: README.md
	$(PANDOC) --toc --toc-depth=3 --citeproc --standalone $< -o $@

resistanceisfutile.pdf: README.md
	$(PANDOC) --toc --toc-depth=3 --citeproc --standalone $< -o $@
